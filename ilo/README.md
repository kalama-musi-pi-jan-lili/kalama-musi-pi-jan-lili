# ilo
tools

## mscz2tokiDECtalk.py

kepeken ni la: lipu MSCZ li lipu DECtalk\
Converts Musescore sheet music into DECtalk syntax

tenpo lili la: mi pali e ilo ni.  ilo kin li kepeken taso e nasin Common Time.  ilo kin li jo ike mute.\
A quick hack I threw together that only works with common time. Probably filled with other bugs too.

ilo Bash la: sina ken kepeken pona e ilo DECtalk\
Output can be directly piped into DECtalk:

```
./mscz2tokiDECtalk.py seme_li_lon.mscz | dectalk -pre '[:phone on]' -
```


sina jo ala e ilo DECtalk la: sina ken kepeken e ilo AEIOU\
If you do not have DECtalk, you can pipe to an AEIOU server (with additional sed subsitutions)
```
curl -L https://tts.cyzon.us/tts?text=$(./mscz2tokiDECtalk.py seme_li_lon.mscz | sed 's/\[/\\[/g; s/]/\\]/g') | aplay
```


![cc-zero.png](../cc-zero.png)

This work is marked with [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
