#!/bin/python3
# This work is marked with CC0 1.0 Universal
import os
import sys
import traceback
import re
from zipfile import ZipFile
from lxml import etree, objectify

# ARPAbet intrepretation of toki pona
tokiSyllable = re.compile('[jklmnpstw]?[aeiou][n]?')

tokiARPAbet = {
    ''   :    '_',
     'a' :   'aa',  'an'  :   'aan',  'e' :   'eh',  'en' :   'ehn',  'i' :   'iy', 'in'  :   'iyn',  'o' :   'ow', 'on'  :   'own',  'u' :   'uw',  'un' :   'uwn',
    'ja' : 'yxaa', 'jan'  : 'yxaan', 'je' : 'yxeh', 'jen' : 'yxehn',                                 'jo' : 'yxow', 'jon' : 'yxown', 'ju' : 'yxuw', 'jun' : 'yxuwn',
    'ka' :  'kaa', 'kan'  :  'kaan', 'ke' :  'keh', 'ken' :  'kehn', 'ki' :  'kiy', 'kin' :  'kiyn', 'ko' :  'kow', 'kon' :  'kown', 'ku' :  'kuw', 'kun' :  'kuwn',
    'la' :  'laa', 'lan'  :  'laan', 'le' :  'leh', 'len' :  'lehn', 'li' :  'liy', 'lin' :  'liyn', 'lo' :  'low', 'lon' :  'lown', 'lu' :  'luw', 'lun' :  'luwn',
    'ma' :  'maa', 'man'  :  'maan', 'me' :  'meh', 'men' :  'mehn', 'mi' :  'miy', 'min' :  'miyn', 'mo' :  'mow', 'mon' :  'mown', 'mu' :  'muw', 'mun' :  'muwn',
    'na' :  'naa', 'nan'  :  'naan', 'ne' :  'neh', 'nen' :  'nehn', 'ni' :  'niy', 'nin' :  'niyn', 'no' :  'now', 'non' :  'nown', 'nu' :  'nuw', 'nun' :  'nuwn',
    'pa' :  'paa', 'pan'  :  'paan', 'pe' :  'peh', 'pen' :  'pehn', 'pi' :  'piy', 'pin' :  'piyn', 'po' :  'pow', 'pon' :  'pown', 'pu' :  'puw', 'pun' :  'puwn',
    'sa' :  'saa', 'san'  :  'saan', 'se' :  'seh', 'sen' :  'sehn', 'si' :  'siy', 'sin' :  'siyn', 'so' :  'sow', 'son' :  'sown', 'su' :  'suw', 'sun' :  'suwn',
    'ta' :  'taa', 'tan'  :  'taan', 'te' :  'teh', 'ten' :  'tehn',                                 'to' :  'tow', 'ton' :  'town', 'tu' :  'tuw', 'tun' :  'tuwn',
    'wa' :  'waa', 'wan'  :  'waan', 'we' :  'weh', 'wen' :  'wehn', 'wi' :  'wiy', 'win' :  'wiyn' 
}

bpm = 120;
durations = {
    '16th'     :1/4,
    'sixteenth':1/4,
    'eighth'   :1/2 ,
    'quarter'  :1 ,
    'half'     :2 ,
    'whole'    :4   ,
    'measure'  :4   # Stupid Hack, does not work for uncommon time
}

def computeDuration(durationType, duration):
    return round(durations[durationType] * 60/bpm * 1000);

def usage():
    print("Usage: mscx2tokidectalk.py [INPUT.mscx]")

def main(args):

    song = ''

    if len(args) != 2:
        usage()
        exit()
    
    try:
        filepath = args[1]
        filename = os.path.basename(filepath)
        with ZipFile(filepath, 'r') as zipf:
            with zipf.open(filename[:-1] + 'x', 'r') as f:
                xml = f.read()
            
        mscxRoot = objectify.fromstring(xml)
        mscxTree = etree.ElementTree(mscxRoot)
        staffs = mscxRoot.findall('Score/Staff')
        # Loop over staffs
        for staff in staffs:
            # Loop over measures
            for measure in staff.iterchildren():
                # Loop over measure elements
                for e in measure.iterchildren():
                    # Debug
                    # print(mscxTree.getpath(e))
    
                    # Read Chords and Rests
                    try:
                        if( e.tag == 'Chord' or e.tag == 'Rest' ):
                            # Get ARPAbet syllable
                            lyrics = e.findtext('Lyrics/text') or ''
                            syllable = tokiSyllable.search(lyrics).group(0) if tokiSyllable.search(lyrics) else ''
                            arpaSyllable = tokiARPAbet[syllable]
                            # Get duration
                            duration = computeDuration(e.findtext('durationType'), e.findtext('duration'))
                            # Get Pitch
                            pitch = int( e.findtext('Note/pitch') or 47 )-47
                            # Append syllable
                            song = song + f'[{arpaSyllable}<{duration},{pitch}>]'
                    except:
                        raise(traceback.format_exc());
    
                # LineBreaks
                if( measure.findtext('LayoutBreak/subtype') == 'line'):
                    song = song + '\n'
                        
        print(song)
        
    except IOError:
        usage()
        exit()
        
    except:
        print(traceback.format_exc())
        exit()

main(sys.argv)
